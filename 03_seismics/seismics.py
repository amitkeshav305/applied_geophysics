import numpy as np
def drawWiggle(ax, x, t, xoffset=0.0,
               posColor='tab:red', negColor='tab:blue', alpha=0.5, **kwargs):
    wiggle, = ax.plot(x + xoffset, t, color='black', **kwargs)

    if len(t) > 1:
        tracefill = np.array(x)
        tracefill[0] = 0.0
        tracefill[-1] = 0.0
        tracefill[np.nonzero(x > x[0])] = 0
        fill, = ax.fill(tracefill + xoffset, t, color=negColor,
                        alpha=alpha, linewidth=0)

        tracefill = np.array(x)
        tracefill[0] = 0.0
        tracefill[-1] = 0.0
        tracefill[np.nonzero(x < x[0])] = 0
        fill, = ax.fill(tracefill + xoffset, t, color=posColor,
                        alpha=alpha, linewidth=0)


import matplotlib.pyplot as plt

from IPython.display import set_matplotlib_formats
set_matplotlib_formats("svg")

def plot_seismogram(ax, x, t, data):
    scale = x.max() / len(x) * 0.75
    for i, xi in enumerate(x):
        drawWiggle(ax, data[:,i]*scale, t, xoffset=xi, lw=0.25, alpha=0.4)
    ax.set_xlim(0, x.max())
    ax.set_ylim(t.max(), 0)
    ax.set_ylabel("Two-way travelime (s)")
    ax.set_xlabel("Offset (m)")
